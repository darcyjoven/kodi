package main

import (
	"bufio"
	"io/fs"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"time"
)

type m3u8 struct {
	channel string
	url     string
}

func main() {

	var (
		m3ready  []m3u8
		m3result []m3u8
		m3ufile  = regexp.MustCompile(`.*\.(m3u|m3u8)`)
	)
	filepath.Walk("./m3u8", func(path string, info fs.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}
		if m3ufile.MatchString(path) {
			// err := setnewline(info.Name())
			// if err != nil {
			// 	log.Println(err)
			// 	return err
			// }
			temp, err := read(path)
			if err != nil {
				log.Println(err)
				return err
			}
			m3ready = append(m3ready, temp...)
		}
		return nil
	})
	m3result = check(m3ready)
	// 写入新文件
	_, err := os.Open("oklist.m3u8")
	if err == nil {
		err := os.Remove("oklist.m3u8")
		if err != nil {
			log.Println(err)
			return
		}
	}
	file, err := os.OpenFile("oklist.m3u8", os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		log.Println(err)
		return
	}
	defer file.Close()
	writer := bufio.NewWriter(file)
	_, err = writer.WriteString("#EXTM3U\r\n")
	if err != nil {
		log.Println(err)
		return
	}
	for _, v := range m3result {
		_, err := writer.WriteString(v.channel)
		if err != nil {
			log.Println(err)
			return
		}
		_, err = writer.WriteString(v.url)
		if err != nil {
			log.Println(err)
			return
		}
	}
	writer.Flush()
}

func check(in []m3u8) []m3u8 {
	var (
		data []m3u8
		max  = len(in) - 1
	)
	client := http.Client{Timeout: 3 * time.Second}

	for i, v := range in {
		line := strings.ReplaceAll(v.url, "\r\n", "")
		line = strings.ReplaceAll(v.url, "\n", "")
		_, ok := client.Get(line)
		if ok == nil {
			data = append(data, v)
		}
		log.Printf("chanel is %s,url is %s \n", v.channel, v.url)
		log.Printf("check %d of %d,result %s", i, max, ok)
	}
	return data
}

func read(file string) ([]m3u8, error) {
	var (
		EXTM3U = regexp.MustCompile(`.*#EXTM3U.*`)
		EXTINF = regexp.MustCompile(`#EXTINF.*`)
		data   []m3u8
	)

	f, err := os.Open(file)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	buf := bufio.NewReader(f)
	for {
		var temp m3u8
		line, err := buf.ReadString('\n')
		if err != nil {
			if err.Error() == "EOF" {
				break
			}
			log.Println(err)
			return nil, err
		}
		if EXTM3U.MatchString(line) {
			continue
		}
		if EXTINF.MatchString(line) {
			temp.channel = line
			line, err = buf.ReadString('\n')
			if err != nil {
				if err.Error() != "EOF" {
					log.Println(err)
					return nil, err
				}
			}
			temp.url = line
			data = append(data, temp)
		}
	}
	return data, nil
}
